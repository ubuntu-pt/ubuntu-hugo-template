@João Luz: Para tirares ideias
https://github.com/gohugoio/hugoThemes#themetoml

# Site baseado na framework Hugo e Vanilla framework

[Hugo](https://gohugo.io)


[Vanilla framework](https://vanillaframework.io/)

# Instalar via LXD

## Instalar o LXD
[Instalar LXD](https://canonical.com/lxd/install)

Executar o comando `sudo snap install lxd` para instalar o LXD via "snap".

Exemplo de output de execução:

```bash
$ sudo snap install lxd
[sudo] password for utilizador:
lxd (5.21/stable) 5.21.1-d46c406 from Canonical✓ installed
```

## Inicializar o LXD

Para a inicialização do LXD, executar o comando  `lxd init` Servem as configurações por omissão, **exceto** na pergunta sobre IPv6,
 à qual se recomenda responder "none" (sem as aspas). Exemplo de execução:

```bash
$ lxd init
Would you like to use LXD clustering? (yes/no) [default=no]:
Do you want to configure a new storage pool? (yes/no) [default=yes]:
Name of the new storage pool [default=default]:
Name of the storage backend to use (powerflex, zfs, btrfs, ceph, dir, lvm) [default=zfs]:
Create a new ZFS pool? (yes/no) [default=yes]:
Would you like to use an existing empty block device (e.g. a disk or partition)? (yes/no) [default=no]:
Size in GiB of the new loop device (1GiB minimum) [default=11GiB]:
Would you like to connect to a MAAS server? (yes/no) [default=no]:
Would you like to create a new local network bridge? (yes/no) [default=yes]:
What should the new bridge be called? [default=lxdbr0]:
What IPv4 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]:
What IPv6 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]: none
Would you like the LXD server to be available over the network? (yes/no) [default=no]:
Would you like stale cached images to be updated automatically? (yes/no) [default=yes]:
Would you like a YAML "lxd init" preseed to be printed? (yes/no) [default=no]:
```



## Criar 'Container'

Executar o comando seguinte para criar um container chamado "ubuntu-pt-site", a partir da imagem Ubuntu mais recente que estiver disponível:

```bash
lxc launch ubuntu: ubuntu-pt-site
```
É normal que a execução do comando anterior demore alguns minutos a ser concluída, dado que a imagem Ubuntu mais recente é relativamente grande.


## Adicionar regra de ufw na máquina host

Se estivermos a usar "ufw" (uncomplicated firewall) na máquina host, será necessário adicionarmos algumas regras de "ufw" nessa máquina host para que se faça a comunicação com o interface bridge do LXD ("lxdbr0"). Para isso, deveremos executar o seguinte comando:

```bash
sudo ufw allow in on lxdbr0 && sudo ufw route allow in on lxdbr0 && sudo ufw route allow out on lxdbr0
```

Referência:

https://discuss.linuxcontainers.org/t/lxd-bridge-doesnt-work-with-ipv4-and-ufw-with-nftables/10034/17


## Entrar no container

Executar o comando seguinte para aceder à shell do container:

```bash
lxc shell ubuntu-pt-site
```

## Actualizar o sistema

O comando anterior colocou-nos dentro do container. Executar o comando seguinte para actualizar os pacotes 
de software que estão dentro do container e fazer reboot do mesmo:

```bash
apt update && apt full-upgrade && snap refresh && reboot
```

# Instalar o Hugo

Começar por voltar a entrar no container (dado que o "reboot" executado no comando anterior nos terá 
forçado a sair do container), executando o seguinte comando:

```bash
lxc shell ubuntu-pt-site
```

Agora vamos executar o comando para instalar o Hugo:

```bash
snap install hugo --channel=extended/stable
```

# Clonar repositório

## Gerar Chave ssh e colocar no gitlab

### Gerar Chave ssh

```bash
ssh-keygen
```

### Obter a chave

Para vermos a chave pública ed25519 gerada, executar o seguinte comando:


cat ~/.ssh/id_ed25519.pub 

O output será algo semelhante ao seguinte:

Algo assim:
```bash
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMLtx9YSsJsW9435DDSFCktF5/W0sOmsf1+1cZy4/XXXX centrolinux@centrolinux-LT1000
```

### Colocar a chave pública gerada no nosso perfil no GitLab

A seguinte secção da documentação do GitLab explica como colocar a nossa chave pública SSH no GitLab:

https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account


## Instalar git

Ainda dentro do container, executar o seguinte comando para instalar o "git":

```bash
apt install git-all
```


## Clonar repositório

```bash
git clone git@gitlab.com:ubuntu-pt/ubuntu-hugo-template.git
```

Depois de executar o comando anterior, executar o comando seguinte para navegar para a directoria do repositório:

```bash
cd ubuntu-hugo-template
```

# Arrancar com o Hugo
```bash
hugo server --logLevel debug
```

# Arrancar com o Hugo para aceder remotamente (a partir do host)

Para arrancarmos o Hugo, dentro do container, para podermos aceder remotamente (a partir da máquina host), executar o seguinte comando na shell do container:

```bash
hugo serve --bind <Ip do Container> --baseURL http://<Ip do Container> -p 1313
```

Tipicamente, o endereço IP do Container será o endereço começado por '10.' que pode ser obtido, com um comando "ip address show" executado dentro do container.

Por exemplo, o seguinte comando, quando executado dentro do Container, devolve o endereço IP:

```bash
ip address show | grep 'inet' | grep '10' | awk '{print $2}' | awk -F\/ '{print $1}'
```

Admitindo que o comando anterior devolva o resultado `10.203.115.177` então o comando a executar, para arrancar o Hugo, seria o seguinte:

Exemplo:

```bash
hugo serve --bind 10.203.115.177 --baseURL http://10.203.115.177 -p 1313
```

# Arrancar com o Hugo em DEV
```bash
./run_dev.sh
```

# Arrancar com o Hugo em PRD
```bash
./run_prd.sh
```

# Link para o site

Na máquina host, poderemos aceder ao site através do URL http://<Ip do Container>:1313


# Criar página genérica
```bash
hugo new <Local e nome da página>
```
Exemplo:

```bash
hugo new about/about.md
```

# Criar página para redirecionar
```bash
HUGO_EXTERNAL_URL="<Link externo>" hugo new --kind <Idioma>/externalRedirect <Local e nome da página><Idioma>.md
```
Exemplo:

```bash
HUGO_EXTERNAL_URL="https://ubuntu.com" hugo new --kind pt/externalRedirect about/ubuntu.md
```



# Criar conteudo estático com o Hugo na pasta public
```bash
hugo
```

# Shortcodes Templates
[shortcode-templates](https://gohugo.io/templates/shortcode-templates/)

Quando se cria novos shortcodes, deve ter-se a preocupação de os fazer com Acessibilidade 
[ARIA](https://www.w3.org/WAI/ARIA/apg/)

### timedisplay
Formata a data conforme o idioma do teu browser

Como usar no markdown:

```bash
 {{< timedisplay datetime="2022-08-28T23:55:00+01:00" display="date" >}}
```

Como usar directamente no HTML:

```html
 <u class="timedisplay" display="date" style="text-decoration-style: dotted;">2022-08-28T23:55:00+01:00</u></p>
```

Opções no parametro "display":
- date
- time
- qualquer coisa => Retorna datetime

### accordion
Expandir ou recolher uma secção (pode ser texto)

Como usar no markdown:

```bash
{{< accordion title="<Titulo do acordeão>" id="<Id Unico>" expanded="<true|false>" >}}
	<O que fica dentro do acordeão>
{{< /accordion >}}
```
Exemplo:

```bash
{{< accordion title="Comunidade Ubuntu Portugal" id="idCUP" expanded="false" >}}
	Este exemplo de acordeão veio do git Comunidade Ubuntu Portugal Template
{{< /accordion >}}
```

### button
Botão que suporta mini icon a esquerda

```bash
{{< button text="<Texto do Botão>" href="<link do botão>" icon="<icon do botão>" >}}
```
Exemplo:

```bash
{{< button text="Botão com icon de Link Externo" href="https://ubuntu-pt.org/" icon="external-link" >}}
```

### profile
Template para profile

```bash
{{< profile fotografia="<Fotografia>" nome="<Nome>" bold="<Frase que quero destacadar>" desc="<Aqui é uma mini descrição>" >}}
```
Exemplo:

```bash
{{< profile fotografia="/images/logo.simplificado.svg" nome="Princeps Civium" bold="O primeiro dos cidadãos" desc="A nova estrutura política criada por Augusto designa-se por \"principado\", sendo o chefe do império designado por princeps civium" >}}
```




# Alterações necessárias nos novos projectos

### Ficheiro config.yaml

Todas as que fizerem sentido alterar

### Ficheiro i18n/pt-pt.toml

Na secção "Especifico"


### Ficheiro /static/CNAME

Colocar o Domínio.
Exemplo:

```bash
 ubuconpt2023.ubuntu-pt.org
```

### Imagem /static/images/og/ogimage.png
Alterar a imagem que se quer usar como fundo na imagem que vai ser usada nas redes socias


### Ficheiro content/information/about.md

Na descrição no cabeçalho


### Ficheiro content/privacy-policy/index.md

Procurar por "COLOCAR TEXTO"


### Ficheiro layouts/index.html

Procurar por "COLOCAR TEXTO"

### Em todo o site

Procurar por "TRADUCAO_EN:"


#og_img_generator
Open Graph image generator => Gerador de imagens para serem usadas nas redes socias

[opengraph](https://www.opengraph.xyz/) => Para vizualisar o resultado

[The Open Graph protocol](https://ogp.me/) => Regras do Open Graph

### Indicações

### Instalar python3
Construção 
	-> Instalar o modulo imgkit
	
	```bash
	sudo apt install wkhtmltopdf
	```
	 


#Cores que usamos no site
```bash
 #b5185f => ~Vermelho violeta médio
 #700c69 => ~Roxo
 #270073 => ~Índigo
```



# Sintaxe básica do readme.md
[Sintaxe básica](https://docs.github.com/pt/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax#quoting-text)


# License
Code: MIT, Contents: CC BY 4.0
