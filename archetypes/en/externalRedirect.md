---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
externalRedirect: {{ getenv "HUGO_EXTERNAL_URL" }}
---

TRADUCAO_EN:A redirecionar para a página {{ getenv "HUGO_EXTERNAL_URL" }} ...

TRADUCAO_EN:[Pressione para redirecionar manualmente]({{ getenv "HUGO_EXTERNAL_URL" }})
