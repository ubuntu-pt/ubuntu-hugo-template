#!/bin/bash

echo "-----------------------------------"
echo "1 - Remover a pasta public"
echo "-----------------------------------"
rm -r public

echo "-----------------------------------"
echo "2 - Atribuir thema"
echo "-----------------------------------"
sed -i 's/#theme: NOME DO TEMA/theme: Comunity/' config.yaml

echo "-----------------------------------"
echo "3 - Criar a pasta public"
echo "-----------------------------------"
hugo

# https://discourse.gohugo.io/t/global-404-page-for-a-multilingual-site/20979
# https://github.com/gohugoio/hugo/issues/5161
echo "-----------------------------------"
echo "4 - Copiar o 404.html"
echo "-----------------------------------"
cp public/pt/404.html public/404.html

echo "-----------------------------------"
echo "5 - Lançar o servidor"
echo "-----------------------------------"
hugo server --logLevel debug
